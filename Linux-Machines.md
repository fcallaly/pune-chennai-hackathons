# Chennai/Pune Linux Info
An individual linux machine has been created for each participant. These machines will be used to cover deployment concepts.


## Machine Assignments

### Chennai

Take a note of your windows remote desktop number.

E.g. if your windows remote desktop hostname is: citichennai34.conygre.com
Then 34 is your remote desktop number.

**Replace the X** in the hostname below to get your Linux hostname:

chennaidevopsX.conygre.com


### Pune

Take a note of your windows remote desktop number.

E.g. if your windows remote desktop hostname is: citipune34.conygre.com
Then 34 is your remote desktop number.

**Replace the X** in the hostname below to get your Linux hostname:


There are two batches of pune linux machines (Note the a and b in the hostnames!!)

* one for remote desktop numbers less than or equal to 60 - the base linux hostname is punedevopsaX.conygre.com
* one for remote desktop numbers greater than 60 - subtract 60 from your remote desktop number and the base linux hostname is punedevopsbX.conygre.com



To repeat the above in pseudo-code!

```
IF your remote desktop number <= 60 :
    X = your remote desktop number
    your linux hostname is:  punedevopsaX.conygre.com

Else:
    X = (your remote desktop number - 60)
    your linux hostname is:  punedevopsbX.conygre.com
```

## Login Details

ssh login (ask the instructors)